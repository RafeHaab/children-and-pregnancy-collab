﻿using UnityEngine;
using Verse;
using Verse.AI;
using RimWorld;
using System.Collections.Generic;
using System.Diagnostics;

namespace RimWorldChildren
{

	public class WorkGiver_TakeBabyToBedAndFeed : WorkGiver_Scanner
	{
		//
		// Properties
		//
		public override PathEndMode PathEndMode {
			get {
				return PathEndMode.Touch;
			}
		}
		public override ThingRequest PotentialWorkThingRequest {
			get {
				return ThingRequest.ForGroup (ThingRequestGroup.Pawn);
			}
		}

		//
		// Methods
		//
		public override bool HasJobOnThing (Pawn pawn, Thing t, bool forced = false)
		{
			Pawn baby = t as Pawn;
			if (baby == null || baby == pawn) {
				return false;
			}
			if (!baby.RaceProps.Humanlike || baby.ageTracker.CurLifeStageIndex > AgeStage.Toddler) {
				return false;
			}
			if (!pawn.CanReserveAndReach (t, PathEndMode.ClosestTouch, Danger.Deadly, 1, -1, null, forced)) {
				return false;
			}
			Building_Bed crib = ChildrenUtility.FindCribFor(baby, pawn) ?? RestUtility.FindBedFor(baby, pawn, false, false);
			if ( crib == null ){
				JobFailReason.Is("NoCrib".Translate());
				return false;
			}
			// Is baby hungry?
			if (baby.needs.food == null || baby.needs.food.CurLevelPercentage > baby.needs.food.PercentageThreshHungry + 0.02f){
				return false;
			}
			/*if (!FeedPatientUtility.ShouldBeFed(baby)){
				return false;
			}*/
			LocalTargetInfo target = t;
			if (!pawn.CanReserve(target, 1, -1, null, forced)){
				return false;
			}
			Thing thing;
			ThingDef thingDef;
			if (!FoodUtility.TryFindBestFoodSourceFor(pawn, baby, baby.needs.food.CurCategory == HungerCategory.Starving, out thing, out thingDef, false)){
				JobFailReason.Is("NoFood".Translate(), null);
				return false;
			}

			return true;
		}
		
		public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false)
		{
			var baby = (Pawn)t;
			Building_Bed crib = ChildrenUtility.FindCribFor(baby, pawn) ?? RestUtility.FindBedFor(baby, pawn, false, false);
			
			Thing thing;
			ThingDef thingDef;
			Thing foodInInv = FoodUtility.BestFoodInInventory(pawn, baby, FoodPreferability.MealSimple);
			if(foodInInv == null){
				FoodUtility.TryFindBestFoodSourceFor(pawn, baby, baby.needs.food.CurCategory == HungerCategory.Starving, out thing, out thingDef, false);
			}
			else{
				thing = foodInInv;
				thingDef = thing.def;
			}
			if (crib != null && thing != null)
			{
				float nutrition = FoodUtility.GetNutrition(thing, thingDef);
				var feedBaby = new Job(DefDatabase<JobDef>.GetNamed("TakeBabyToBedAndFeed"), thing, baby, crib){
					count = FoodUtility.WillIngestStackCountOf(baby, thingDef, nutrition)
				};
				return feedBaby;
			}
			return null;
		}
	}
	
	public class JobDriver_TakeBabyToBedAndFeed : JobDriver
	{
		private const TargetIndex FoodSourceInd = TargetIndex.A;

		private const TargetIndex DelivereeInd = TargetIndex.B;
		
		private const TargetIndex CribInd = TargetIndex.C;

		private const float FeedDurationMultiplier = 1.5f;

		protected Thing Food{
			get{
				return job.targetA.Thing;
			}
		}

		protected Pawn Deliveree{
			get{
				return (Pawn)job.targetB.Thing;
			}
		}
		
		protected Building_Bed Crib{
			get{
				return (Building_Bed)job.targetC.Thing;
			}
		}

		public override string GetReport()
		{
			if (job.GetTarget(TargetIndex.A).Thing is Building_NutrientPasteDispenser && Deliveree != null)
			{
				return job.def.reportString.Replace("TargetA", ThingDefOf.MealNutrientPaste.label).Replace("TargetB", Deliveree.LabelShort);
			}
			return base.GetReport();
		}

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			Pawn pawn = this.pawn;
			Job job = this.job;
			if (!pawn.Reserve(new LocalTargetInfo(Deliveree), job, 1, -1, null, errorOnFailed) || !pawn.Reserve(new LocalTargetInfo(Crib), job, 1, -1, null, errorOnFailed)){
				return false;
			}
			if (!(TargetThingA is Building_NutrientPasteDispenser) && (this.pawn.inventory == null || !this.pawn.inventory.Contains(TargetThingA)))
			{
				pawn = this.pawn;
				job = this.job;
				if (!pawn.Reserve(new LocalTargetInfo(Food), job, 1, -1, null, errorOnFailed))
				{
					return false;
				}
			}
			return true;
		}

		[DebuggerHidden]
		protected override IEnumerable<Toil> MakeNewToils()
		{
			
			this.FailOnDestroyedNullOrForbidden(DelivereeInd);
			this.FailOnDespawnedNullOrForbidden(CribInd);
			yield return Toils_Reserve.Reserve(CribInd);
			this.FailOn(() => Deliveree.needs.food.CurLevelPercentage > Deliveree.needs.food.PercentageThreshHungry + 0.02f);
			
			// Find the food
			if (pawn.inventory != null && pawn.inventory.Contains(Food))
			{
				yield return Toils_Misc.TakeItemFromInventoryToCarrier(pawn, FoodSourceInd);
			}
			else if (TargetThingA is Building_NutrientPasteDispenser)
			{
				yield return Toils_Goto.GotoThing(FoodSourceInd, PathEndMode.InteractionCell).FailOnForbidden(FoodSourceInd);
				yield return Toils_Ingest.TakeMealFromDispenser(FoodSourceInd, pawn);
			}
			else
			{
				yield return Toils_Goto.GotoThing(FoodSourceInd, PathEndMode.ClosestTouch).FailOnForbidden(FoodSourceInd);
				yield return Toils_Ingest.PickupIngestible(FoodSourceInd, Deliveree);
			}
			// Put the food in our pocket
			yield return Toils_General.PutCarriedThingInInventory();
			
			// Go to the baby
			yield return Toils_Goto.GotoThing(DelivereeInd, PathEndMode.Touch);
			
			// Make sure crib hasn't been destroyed
			yield return Toils_Haul.StartCarryThing(DelivereeInd);
			yield return new Toil{ initAction = delegate{ Log.Message("Benis 1"); }, };
			yield return Toils_Goto.GotoThing(CribInd, PathEndMode.Touch);
			yield return Toils_Reserve.Release(CribInd);
			yield return new Toil
			{
				initAction = delegate
				{
					Building_Bed DropBed = Crib;
					IntVec3 position = DropBed.Position;
					Thing thing;
					pawn.carryTracker.TryDropCarriedThing(position, ThingPlaceMode.Direct, out thing, null);
					if (!DropBed.Destroyed && (DropBed.owners.Contains(Deliveree) || (DropBed.Medical && DropBed.AnyUnoccupiedSleepingSlot) || Deliveree.ownership == null)){
						Deliveree.mindState.Notify_TuckedIntoBed();
					}
				},
				defaultCompleteMode = ToilCompleteMode.Instant
			};
			yield return new Toil{
				initAction = delegate{
					PawnUtility.ForceWait(Deliveree, Mathf.RoundToInt((float)Food.def.ingestible.baseIngestTicks * FeedDurationMultiplier), pawn, true);
				}
			};
			yield return Toils_Misc.TakeItemFromInventoryToCarrier(pawn, FoodSourceInd);
			yield return Toils_Ingest.ChewIngestible(Deliveree, FeedDurationMultiplier, FoodSourceInd).FailOnCannotTouch(DelivereeInd, PathEndMode.Touch);
			yield return Toils_Ingest.FinalizeIngest(Deliveree, TargetIndex.A);
		}
	}
}